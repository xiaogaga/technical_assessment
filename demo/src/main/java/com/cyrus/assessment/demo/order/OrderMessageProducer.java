package com.cyrus.assessment.demo.order;

import org.apache.rocketmq.client.exception.MQClientException;
import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.common.message.Message;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import jakarta.annotation.PostConstruct;
import jakarta.annotation.PreDestroy;
import java.nio.charset.StandardCharsets;

@Component
public class OrderMessageProducer {

    private static final Logger logger = LoggerFactory.getLogger(OrderMessageProducer.class);

    private DefaultMQProducer producer;

    @PostConstruct
    public void start() throws MQClientException {
        producer = new DefaultMQProducer("OrderProducerGroup");
        producer.setNamesrvAddr("localhost:9876"); // Replace with your RocketMQ nameserver address
        producer.start();
        logger.info("OrderMessageProducer started.");
    }

    @PreDestroy
    public void shutdown() {
        if (producer != null) {
            producer.shutdown();
            logger.info("OrderMessageProducer shutdown.");
        }
    }

    public void sendOrderCreatedMessage(Order order) {
        try {
            String orderJson = convertOrderToJson(order); // Convert order object to JSON string
            Message message = new Message("OrderTopic", "OrderCreated", orderJson.getBytes(StandardCharsets.UTF_8));
            SendResult sendResult = producer.send(message);
            logger.info("Order created message sent: {}", sendResult);
        } catch (Exception e) {
            logger.error("Failed to send order created message.", e);
        }
    }

    private String convertOrderToJson(Order order) {
        // Use your preferred JSON library to convert the order object to a JSON string
        // Here we use Jackson as an example:
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            return objectMapper.writeValueAsString(order);
        } catch (JsonProcessingException e) {
            logger.error("Error converting order to JSON", e);
            return null;
        }
    }
}
