package com.cyrus.assessment.demo.order;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/order")
public class OrderController {

    @Autowired
    private OrderService orderService;

    @PostMapping("/placeOrder")
    public ResponseEntity<OrderResponse> placeOrder(@RequestBody OrderRequest orderRequest) {

        // check parameter
        if (orderRequest == null || orderRequest.getUserId() == null || orderRequest.getProductCode() == null || orderRequest.getQuantity() <= 0) {
            return ResponseEntity.badRequest().body(new OrderResponse(ErrorCode.INVALID_PARAMETER));
        }
        
        // check user exist (skip)

        // check product exist and stock enough and place order
        return orderService.placeOrder(orderRequest);
    }

    @PostMapping("/payOrder")
    public ResponseEntity<OrderResponse> payOrder(@RequestBody String orderId) {

        // check user exist (skip)

        return orderService.payOrder(orderId);
    }
    
}
