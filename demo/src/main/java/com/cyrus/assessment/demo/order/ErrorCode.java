package com.cyrus.assessment.demo.order;

public enum ErrorCode {
    INVALID_PARAMETER("Invalid parameter provided"),
    INSUFFICIENT_STOCK("Insufficient stock"),
    ORDER_NOT_FOUND("Order not found"),
    PAYMENT_FAILED("Payment failed"),
    GENERAL_ERROR("An error occurred"),
    PRODUCT_NOT_FOUND("Product not found"),
    STOCK_NOT_ENOUGH("Stock not enough"), 
    INTERNAL_SERVER_ERROR("Internal server error"), 
    ORDER_ALREADY_PAID("order already paid"), 
    ORDER_CANCELLED("order cancel"), 
    ORDER_UNKNOWN_ERROR("Unknown order error");

    private final String message;

    ErrorCode(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}

