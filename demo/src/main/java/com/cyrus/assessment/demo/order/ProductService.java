package com.cyrus.assessment.demo.order;

import org.springframework.beans.factory.annotation.Autowired;

public class ProductService {

    //Product getProductByCode(String productCode);
    //void deductStock(String productCode, int quantity);


    @Autowired
    private ProductRepository productRepository;

    //@Override
    public Product getProductByCode(String productCode) {
        return productRepository.findByProductCode(productCode);
    }
    
}
