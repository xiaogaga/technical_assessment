package com.cyrus.assessment.demo.order;

public class OrderResponse {
    private String status;
    private String message;
    private Order order;
    private ErrorCode errorCode;

    public OrderResponse() {
    }

    public OrderResponse(String status, String message, Order order) {
        this.status = status;
        this.message = message;
        this.order = order;
    }

    public OrderResponse(ErrorCode errorCode) {
        this.status = "FAIL";
        this.message = errorCode.getMessage();
        this.errorCode = errorCode;
    }

    // Getters and Setters

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public ErrorCode getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(ErrorCode errorCode) {
        this.errorCode = errorCode;
    }
}

