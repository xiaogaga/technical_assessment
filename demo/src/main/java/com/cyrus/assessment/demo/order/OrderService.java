package com.cyrus.assessment.demo.order;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import jodd.net.HttpStatus;

@Service
public class OrderService {

    private static final Logger logger = LoggerFactory.getLogger(OrderService.class);

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private RedisTemplate<String, Object> redisTemplate;

    @Autowired
    private RocketMQTemplate rocketMQTemplate;

    @Transactional(isolation = Isolation.SERIALIZABLE) 
    public ResponseEntity<OrderResponse> placeOrder(OrderRequest orderRequest) {


        // skip param checking and those redis code
        // String productKey = "product:" + orderRequest.getProductCode();
        // Product product = redisTemplate.opsForValue().get(productKey);

        String productKey = orderRequest.getProductCode();
        try {

            Product product = productRepository.findByProductCode(productKey);
            if (product == null) {
                return ResponseEntity.badRequest().body(new OrderResponse(ErrorCode.PRODUCT_NOT_FOUND));
            }
            if (product.getStock() < orderRequest.getQuantity()) {
                return ResponseEntity.badRequest().body(new OrderResponse(ErrorCode.STOCK_NOT_ENOUGH));
            }

            Order order = new Order();
            order.setOrderNumber(generateOrderNo());
            order.setUserId(orderRequest.getUserId());
            order.setProductCode(orderRequest.getProductCode());
            order.setQuantity(orderRequest.getQuantity());
            order.setAmount(product.getPrice());
            order.setTotalAmount(product.getPrice().multiply(new BigDecimal(String.valueOf(order.getQuantity()))));
            order.setOrderTime(LocalDateTime.now());
            order.setStatus("PENDING");

            // deduct stock count
            product.setStock(product.getStock() -  order.getQuantity());
            productRepository.save(product);
            orderRepository.save(order);

            // Cache the product information
            redisTemplate.opsForValue().set("product:" + order.getProductCode(), product);

            // Send a message to RocketMQ
            rocketMQTemplate.convertAndSend("order-topic", "Order placed: " + order.getOrderNumber());

            // return response
            OrderResponse response = new OrderResponse();
            response.setOrder(order);
            return ResponseEntity.ok(response);

        } catch (Exception e) {
            logger.error("Exception: productKey = " + productKey, e);

            OrderResponse response = new OrderResponse();
            response.setMessage("exception: productKey =" + productKey);
            response.setErrorCode(ErrorCode.INTERNAL_SERVER_ERROR);
            return ResponseEntity.badRequest().body(response);
        }

    }

    @Transactional(isolation = Isolation.SERIALIZABLE) 
    public ResponseEntity<OrderResponse> payOrder(String orderNumber) {

        try {
            Order order = orderRepository.findByOrderNumber(orderNumber);
            if (order == null || !"PENDING".equals(order.getStatus())) {
                throw new RuntimeException("Invalid order");
            }

            if (order.getStatus() == "PENDING") {

                order.setStatus("PAID");
                orderRepository.save(order);

                // Send a message to RocketMQ
                rocketMQTemplate.convertAndSend("order-topic", "Order paid: " + order.getOrderNumber());

                OrderResponse response = new OrderResponse();
                response.setOrder(order);
                return ResponseEntity.ok(response);
            } 
            else if (order.getStatus() == "PAID") {
                return ResponseEntity.badRequest().body(new OrderResponse(ErrorCode.ORDER_ALREADY_PAID));
            }
            else if (order.getStatus() == "CANCELLED") {
                return ResponseEntity.badRequest().body(new OrderResponse(ErrorCode.ORDER_CANCELLED));
            }
            else {
                return ResponseEntity.badRequest().body(new OrderResponse(ErrorCode.ORDER_UNKNOWN_ERROR));
            }
        } catch (Exception e) {
            logger.error("Exception: orderNumber = " + orderNumber, e);
            return ResponseEntity.badRequest().body(new OrderResponse(ErrorCode.INTERNAL_SERVER_ERROR));
        }

    }

    public Order getOrder(String orderNumber) {
        return orderRepository.findByOrderNumber(orderNumber);
    }

    @Scheduled(fixedRate = 60000) // Run every minute
    public void cancelUnpaidOrders() {
        List<Order> orders = orderRepository.findByStatus("PENDING");
        for (Order order : orders) {
            restoreStock(order);
        }
    }

    @Transactional(isolation = Isolation.SERIALIZABLE)
    public void restoreStock(Order order) {

        try {
            LocalDateTime now = LocalDateTime.now();
            if (order.getOrderTime().plusMinutes(30).isBefore(now)) {
                order.setStatus("CANCELLED");
                orderRepository.save(order);

                // Restore stock
                Product product = productRepository.findByProductCode(order.getProductCode());
                product.setStock(product.getStock() + order.getQuantity());
                productRepository.save(product);

                // Send a message to RocketMQ
                rocketMQTemplate.convertAndSend("order-topic", "Order cancelled: " + order.getOrderNumber());
            }

        } catch (Exception e) {
            logger.error("Exception: " + order.getOrderNumber(), e);
            throw e;
        }

    }

    public String generateOrderNo() {
        return UUID.randomUUID().toString().replace("-", "");
    }
}
